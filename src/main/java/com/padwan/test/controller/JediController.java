package com.padwan.test.controller;

import com.padwan.test.dto.JediDto;
import com.padwan.test.entity.Jedi;
import com.padwan.test.nativequeryinterfaces.Contador;
import com.padwan.test.nativequeryinterfaces.JedisEMestres;
import com.padwan.test.service.JediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/jedi")
public class JediController {

    @Autowired
    JediService service;

    @GetMapping
    public ResponseEntity<Object> listar() {
        List<JediDto> listarTodos = service.listar();
        return new ResponseEntity(listarTodos, HttpStatus.OK);
    }

    @GetMapping("/jedismestres")
    public ResponseEntity<Object> listarJedisMestres() {
        List<JedisEMestres> listarJedisEMestres = service.listarJedisMestres();
        return new ResponseEntity(listarJedisEMestres, HttpStatus.OK);
    }

    @GetMapping("/mid")
    public ResponseEntity<Object> listarMid() {
        List<JediDto> listarMid = service.listarMid();
        return new ResponseEntity(listarMid, HttpStatus.OK);
    }

    @GetMapping("/status")
    public ResponseEntity<Object> listarStatus() {
        List<Contador> listarContador = service.listarStatus();
        return new ResponseEntity(listarContador, HttpStatus.OK);
    }

    @PostMapping
    public Jedi salvar(@RequestBody Jedi jedi) {
        return service.salvar(jedi);
    }

}
