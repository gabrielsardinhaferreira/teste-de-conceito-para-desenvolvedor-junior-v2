package com.padwan.test.nativequeryinterfaces;

public interface Contador {
    String getStatus();
    Integer getCount();
}
