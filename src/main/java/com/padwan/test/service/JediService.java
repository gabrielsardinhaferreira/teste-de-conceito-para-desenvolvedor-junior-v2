package com.padwan.test.service;

import com.padwan.test.dto.JediDto;
import com.padwan.test.entity.Jedi;
import com.padwan.test.nativequeryinterfaces.Contador;
import com.padwan.test.nativequeryinterfaces.JedisEMestres;
import com.padwan.test.repository.JediRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JediService {
    @Autowired
    JediRepository repository;

    public List<JediDto> listar() {
        List<Jedi> lista = repository.findAll();
        List<JediDto> listaDTO = lista.stream().map(j -> converter(j)).collect(Collectors.toList());
        return listaDTO;
    }

    public List<JedisEMestres> listarJedisMestres() {
        List<JedisEMestres> lista = repository.findByNomeAndStatus();
        return lista;
    }

    public List<JediDto> listarMid() {
        List<Jedi> lista = repository.findByMidichlorians();
        List<JediDto> listaDTO = lista.stream().map(j -> converter(j)).collect(Collectors.toList());
        return listaDTO;
    }

    public List<Contador> listarStatus() {
        List<Contador> lista = repository.findByStatus();
        return lista;
    }

    public Jedi salvar(Jedi jedi) {
        return repository.save(jedi);
    }

    private JediDto converter(Jedi jedi) {
        JediDto jediDTO = new JediDto(jedi);
        return jediDTO;
    }

}
