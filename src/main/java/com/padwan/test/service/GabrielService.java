package com.padwan.test.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GabrielService {

    public List<String> skills(){
        List<String> skills = new ArrayList<>();
        skills.add("force Java");
        skills.add("force Spring");
        skills.add("force Angular");
        skills.add("force Postgre");
        skills.add("master Docker");
        skills.add("master Kubernetes");
        skills.add("Devops");
        return skills;
    }

}
