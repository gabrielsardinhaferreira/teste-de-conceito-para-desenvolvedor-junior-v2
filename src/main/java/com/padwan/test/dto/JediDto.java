package com.padwan.test.dto;

import com.padwan.test.entity.Jedi;
import com.padwan.test.enumerador.StatusJedi;

public class JediDto {

    private Long id;
    private String nome;
    private Integer midichlorians;
    private String status;
    private String mentor;

    public JediDto(Jedi jedi) {
        this.id = jedi.getId();
        this.nome = jedi.getNome();
        this.midichlorians = jedi.getMidichlorians();
        this.status = jedi.getStatus().getDescricao();
        this.mentor = jedi.getMentor();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Integer getMidichlorians() {
        return midichlorians;
    }

    public String getStatus() {
        return status;
    }

    public String getMentor() {
        return mentor;
    }
}
