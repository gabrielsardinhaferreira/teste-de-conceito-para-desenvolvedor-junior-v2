package com.padwan.test.repository;

import com.padwan.test.entity.Jedi;
import com.padwan.test.nativequeryinterfaces.Contador;
import com.padwan.test.nativequeryinterfaces.JedisEMestres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JediRepository extends JpaRepository<Jedi, Long> {

    //Listar todos os mestres Jedis e seus aprendizes;
    @Query(value = "SELECT nome, status FROM jedi", nativeQuery = true)
    List<JedisEMestres> findByNomeAndStatus();

    //Listar todos Jedis cujo midichlorians sejam acima de 9000;
    @Query(value = "SELECT * FROM jedi WHERE jedi.midichlorians > 9000", nativeQuery = true)
    List<Jedi> findByMidichlorians();

    //Listar por categoria, quantos são os Jedis;
    @Query(value = "SELECT status ,count(status) FROM jedi GROUP BY status", nativeQuery = true)
    List<Contador> findByStatus();
}
