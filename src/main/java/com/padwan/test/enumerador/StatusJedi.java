package com.padwan.test.enumerador;

public enum StatusJedi {

    PADAWAN("Padawan"),
    JEDI("Jedi"),
    MESTRE_JEDI("Mestre Jedi");

    private String descricao;

    StatusJedi(String descricao) {
        this.descricao = descricao;
    }

    public String getName() {
        return this.name();
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

    public static StatusJedi getEnum(String descricao) {
        if(descricao == null)
            throw new IllegalArgumentException();
        for(StatusJedi b: values())
            if(descricao.equalsIgnoreCase(b.getDescricao())) return b;
        throw new IllegalArgumentException();
    }


}
